# JIRA CAS 7 JAR Files

This repository includes open source JIRA CAS 7 JAR Files built and ready for use in your JIRA 7 instance. This repository also includes instructions on setting up and configuring CAS with JIRA 7.